'use strict';

var debug = 1;

$(function() {

	var d = new Date(),
		n = d.getMonth();

	// -----------------------------------------------------------------
	// Load SVG sprite
	// Below line is the only one that is required to set up the script

	var date = new Date(),
		refreshAfter = date.getMinutes(), // If set this way script will cache icons for one minute

		loadedSVG = $('body').SVGIconSetLoader({
			debug			: debug,
			iconsFileUrl	: 'icons.svg',
			iconsVersion	: refreshAfter,
		});


	// -----------------------------------------------------------------
	// Some definitions

	var $counter	= $('#counter'),
		$preview	= $('#preview'),
		$name		= $('#name'),
		$code		= $('#code'),
		$search		= $('#search'),
		$showcase	= $('#showcase');


	// -----------------------------------------------------------------
	// Showcase all icons
	// Code below is not necessary for you

	var id,
		className,
		elem,
		iconNum = 0,
		sizes = [];

	function showcaseIcons(filter) {
		$showcase.empty();

		loadedSVG.children('svg').children('g').each(function() {
			className = $(this).attr('class').split('-')[1];
			sizes.push(className);
			elem = $('<div/>', {class: 'group group-' + className});

			$showcase.append(elem);
			elem.html('<h3>Ikony: ' + className + 'px</h3>');

			$(this).children('symbol').each(function() {
				id = $(this).attr('id');
				if (!filter || id.indexOf(filter) > -1) {
					elem.append('<a href="#' + className + '/' + id + '"><svg class="icon-' + className + '"><use xlink:href="#' + id + '"></use></svg></a>');
				}
				iconNum++;
			});
		});
	}

	if (loadedSVG.ajaxPromise) {
		console.log(loadedSVG.ajaxPromise);
		loadedSVG.ajaxPromise.then(function() {
			showcaseIcons();
		});
	}

	else showcaseIcons();

	// -----------------------------------------------------------------
	// Icon click

	$showcase.on('click', 'a', function() {

		$showcase.find('a').removeClass('active');
		$(this).addClass('active');

		var params = this.hash.replace('#', '').split('/');

		$code.find('textarea').html('<svg class="icon-' + params[0] + '"><use xlink:href="#' + params[1] + '"></use></svg>');
		$preview.find('use').attr('xlink:href', '#' + params[1]);
		$name.find('h1').html(params[1]);

		return false;
	});


	// -----------------------------------------------------------------
	// Auto select content of textarea

	$('textarea').click(function() {
		$(this).select();
	});


	// -----------------------------------------------------------------
	// Code copying button

	$code.on('click', 'button', function() {
		$code.find('textarea').select();
		if (document.execCommand('copy')) console.log('Icon code copied');
		else console.error('Couldn\'t copy icon code');
		return false;
	});


	// -----------------------------------------------------------------
	// Colors

	$('#color').on('click', 'a', function() {
		var colorName = this.hash.replace('#','');
		$showcase.removeClass().addClass(colorName);
		console.log('Color changed to ' + colorName);
		return false;
	});


	// -----------------------------------------------------------------
	// Display number of icons

	$counter.find('h1').html(iconNum);


	// -----------------------------------------------------------------
	// Searching

	$search.on('keyup paste', 'input', function(e) {
		showcaseIcons($(this).val());
		console.log('Icons filtered');
	});


	// -----------------------------------------------------------------
	// Searching reset

	$search.on('click', 'button', function() {
		$search.find('input').val('');
		showcaseIcons();
		console.log('Filtering reset');
		return false;
	});


	// -----------------------------------------------------------------
	// Console logs

	console.log('Icons found: ' + iconNum);
	console.log(sizes);

});